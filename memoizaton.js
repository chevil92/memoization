/**
 * Creates a function that memoizes the result of func. If resolver is provided,
 * it determines the cache key for storing the result based on the arguments provided to the memorized function.
 * By default, the first argument provided to the memorized function is used as the map cache key. The memorized values
 * timeout after the timeout exceeds. The timeout is in defined in milliseconds.
 *
 * Example:
 * function addToTime(year, month, day) {
 *  return Date.now() + Date(year, month, day);
 * }
 *
 * const memoized = memoization.memoize(addToTime, (year, month, day) => year + month + day, 5000)
 *
 * // call the provided function cache the result and return the value
 * const result = memoized(1, 11, 26); // result = 1534252012350
 *
 * // because there was no timeout this call should return the memorized value from the first call
 * const secondResult = memoized(1, 11, 26); // secondResult = 1534252012350
 *
 * // after 5000 ms the value is not valid anymore and the original function should be called again
 * const thirdResult = memoized(1, 11, 26); // thirdResult = 1534252159271
 *
 * @param func      the function for which the return values should be cached
 * @param resolver  if provided gets called for each function call with the exact same set of parameters as the
 *                  original function, the resolver function should provide the memoization key.
 * @param timeout   timeout for cached values in milliseconds
 */


function memoize(func, resolver, timeout=0) {
    let memo = new Map()
    let keyFunc = (...arguments) => [...arguments][0] //Use the first argument by default.
    if (typeof resolver !== 'undefined') keyFunc = resolver // use result of resolver function as the key if resolver is here. 
    

    return (...arguments)=> {
        let key = keyFunc(...arguments)
        /*
        array and object could be a key of Map. But they won't work appropriate because of `sameValueZero` algorithm. 
        So we throw exception in that case
        */
        if (["Array", "Object"].includes(key.constructor.name)) throw new TypeError('Key can not be neither Array nor Object');

        /*
        cachedByKey is an array of cached results. We may assume that there could be many different arguments that are able to have the same key. 
        So for handling collisions we will use array and store all results there. 
        */
        let cachedByKey = memo.get(key)   

        // Generate the unique hash for entity base by arguments. We do not use that hash as a key as it could be very long. 
        let hashByArgs = JSON.stringify(arguments) 
        let res;

        //Checking for we have any cached results
        if(typeof cachedByKey !== 'undefined'){
            //If yes, iterate all of them to check result by the unique hash key 
            for(let i = 0; i< cachedByKey.length; i++){
                
                //if there is already cached result for passed arguments
                if(cachedByKey[i].hash == hashByArgs){

                    // and its timeout didn't finish
                    if(cachedByKey[i].time > Date.now()){
                        // use it
                        res = cachedByKey[i].res
                    } else {
                        // otherwise generate result by calling functiion, cache it and return 
                        res = func(...arguments)
                        cachedByKey[i] = {"res": res, "time": Date.now()+timeout, "hash": hashByArgs}
                    }
                    // return result in the for loop to prevent additional checkings futher
                    return res
                }
            }
            // if we didn't return any result before and got to that place it means that we caught collision and we put a new result into the Map 
            res = func(...arguments)
            cachedByKey.push({"res": res, "time": Date.now()+timeout, "hash": hashByArgs})
            return res
        }
        //if there was not any value set by the key. So generationg a result and seting it into the memo Map. 
        res = func(...arguments)
        memo.set(key, [{"res": res, "time": Date.now()+timeout, "hash": hashByArgs}])
        return res
    };
}

module.exports = {
    memoize,
};
