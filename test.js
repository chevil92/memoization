const memoization = require('./memoizaton');
const expect = require('chai').expect;
var sinon = require('sinon');


describe('memoization', function () {
    before(function () { clock = sinon.useFakeTimers(); });
    after(function () { clock.restore(); });

    it('should memoize function result', () =>{
        let returnValue = 5;
        const testFunction =  (key) => returnValue;

        const memoized = memoization.memoize(testFunction, (key) => key, 1000);
        expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(5);

        returnValue = 10;

        // TODO currently fails, should work after implementing the memoize function, it should also work with other
        // types then strings, if there are limitations to which types are possible please state them
        expect(memoized('c544d3ae-a72d-4755-8ce5-d25db415b776')).to.equal(5);
    });


    it('should return a new value by timeout', () =>{
        let now = Date.now()
        const testFunction =  (a,b,c) => Date.now();
        const memoized = memoization.memoize(testFunction, (a,b,c) => a+b+c, 2000);
        let res = memoized(1,2,3)
        expect(memoized(1,2,3)).to.equal(res);
        expect(memoized(1,2,3)).to.equal(res);

        clock.tick(13000)
        expect(memoized(1,2,3)).to.not.equal(res);

        expect(memoized(1,2,3)).to.equal(13000);

    });

    it('should handle same function with different timeout correctly', () =>{
        const testFunction =  (a,b,c) => Date.now();
        const memoized1 = memoization.memoize(testFunction, (a,b,c) => a+b+c, 2000);
        const memoized2 = memoization.memoize(testFunction, (a,b,c) => a+b+c, 10000);

        let res1 = memoized1(1,2,3)
        let res2 = memoized2(1,2,3)
        
        expect(memoized1(1,2,3)).to.equal(res1);
        expect(memoized2(1,2,3)).to.equal(res2);

        clock.tick(3000)
        expect(memoized1(1,2,3)).to.not.equal(res1);

        expect(memoized2(1,2,3)).to.equal(res2);

    });

    it('should handle collisions', () =>{
        const testFunction =  (a,b,c) => (a+b)*c;
        const memoized = memoization.memoize(testFunction, (a,b,c) => a+b+c, 1000);

        //this calls will have the same key 1+2+3=3+2+1=2+1+3 but they have to have a different results (1+2)*3=9, (3+2)*1=5, (1+3)*2=8
        expect(memoized(1,2,3)).to.equal(9);
        expect(memoized(3,2,1)).to.equal(5);
        expect(memoized(1,3,2)).to.equal(8);
    });


    it('should throw exception on array key', () =>{
        const testFunction =  (a) => a[0];
        const memoized = memoization.memoize(testFunction, a => a);
        expect(() => memoized([1,2,3])).to.throw(TypeError, 'Key can not be neither Array nor Object');

    });

});

